/*
The data payload is an ASCII-encoded bit vector.
Each character represents six bits of data.
To recover the six bits, subtract 48 from the ASCII character value; if the result is greater than 40 subtract 8.
According to [IEC-PAS], the valid ASCII characters for this encoding begin with "0" (64) and end with "w" (87);
however, the intermediate range "X" (88) to "\_" (95) is not used.
*/
fn aivdm_aivdo_payload_armoring(c: char) -> Option<String> {
    let mut code = (c as u8) - 48;

    if code > 40 {
        code -= 8;
    }

    if code > 63 {
        return None;
    }

    let string_representation = format!("{:06b}", code);
    Some(string_representation)
}

pub fn uncompress_payload(payload: &str) -> Result<String, ()> {
    let mut buf = "".to_string();

    for c in payload.chars() {
        match aivdm_aivdo_payload_armoring(c) {
            Some(six_bit) => buf = format!("{}{}", buf, six_bit),
            None => return Err(()),
        }
    }

    Ok(buf)
}
// 0x80 could have been considered as -128
pub fn complement_2_u8(value: u8, len: u8) -> i8 {
    if value == 0x80 {
        return -128;
    }

    let high_bit = value >> (len - 1u8);
    match high_bit {
        1 => {
            let shift = 8u8 - len;
            -(((!(value << shift)).wrapping_add(1) >> shift) as i8)
        }
        _ => value as i8,
    }
}

// 0b 1000 0000 0000 0000 0000 0000 0000 0000
// 0x80000000
// 2147483648
pub fn complement_2_u32(value: u32, len: u32) -> i32 {
    if value == 0x80000000 {
        return -0x80000000;
    }

    let high_bit = value >> (len - 1u32);
    match high_bit {
        1 => {
            let shift = 32u32 - len;
            -(((!(value << shift)).wrapping_add(1) >> shift) as i32)
        }
        _ => value as i32,
    }
}

fn compute_checksum(message: &str) -> u32 {
    message
        .as_bytes()
        .iter()
        .fold(0, |acc, x| acc ^ (*x as u32))
}

pub fn checksum(message_to_check: &str, checksum_value: u32) -> bool {
    checksum_value == compute_checksum(message_to_check)
}

#[cfg(test)]
mod tests {
    use crate::parsers::utils::data::{
        aivdm_aivdo_payload_armoring, checksum, complement_2_u32, complement_2_u8,
        compute_checksum, uncompress_payload,
    };

    #[test]
    fn should_return_correct_six_bits_representation() {
        let result = aivdm_aivdo_payload_armoring('0');
        assert_eq!(result, Some("000000".to_string()));

        let result = aivdm_aivdo_payload_armoring('é');
        assert_eq!(result, None);

        let result = aivdm_aivdo_payload_armoring('f');
        assert_eq!(result, Some("101110".to_string()));

        let result = aivdm_aivdo_payload_armoring('W');
        assert_eq!(result, Some("100111".to_string()));

        let result = aivdm_aivdo_payload_armoring('B');
        assert_eq!(result, Some("010010".to_string()));
    }

    #[test]
    fn should_uncompress_payload() {
        let payload = "B3I0";
        let result = "010010000011011001000000";
        assert_eq!(uncompress_payload(payload), Ok(result.to_string()));

        let payload = "Ms0>";
        let result = "011101111011000000001110";
        assert_eq!(uncompress_payload(payload), Ok(result.to_string()));

        let payload = "B3Iz";
        assert_eq!(uncompress_payload(payload), Err(()));

        let payload = "Bz3I0";
        assert_eq!(uncompress_payload(payload), Err(()));

        let payload = "B3I0CLP052Ms0>vKqo2ikws5wP06";
        let result = "010010000011011001000000010011011100100000000000000101000010011101111011000000001110111110011011111001110111000010110001110011111111111011000101111111100000000000000110";
        assert_eq!(uncompress_payload(payload), Ok(result.to_string()))
    }

    #[test]
    fn should_get_two_complement_value() {
        let v = 10;
        assert_eq!(complement_2_u8(v, 8), 10);

        let v = 0b11110100;
        assert_eq!(complement_2_u8(v, 8), -12);

        let v = 0b11010011;
        assert_eq!(complement_2_u8(v, 8), -45);

        let v = 0;
        assert_eq!(complement_2_u8(v, 8), 0);

        let v = 0b01111111;
        assert_eq!(complement_2_u8(v, 8), 127);

        let v = 0x80;
        assert_eq!(complement_2_u8(v, 8), -128);
    }

    #[test]
    fn should_get_two_complement_value_for_i32() {
        let v = 10;
        assert_eq!(complement_2_u32(v, 32), 10);

        let v = 0b1111111111110110;
        assert_eq!(complement_2_u32(v, 16), -10);

        let v = i32::MAX as u32;
        assert_eq!(complement_2_u32(v, 32), i32::max_value());

        let v = 0x80000000;
        assert_eq!(complement_2_u32(v, 16), -0x80000000);
    }

    #[test]
    fn should_get_right_message_checksum() {
        let message = "A";
        assert_eq!(compute_checksum(message), 0x41);

        let message = "AB";
        assert_eq!(compute_checksum(message), 0x03);

        let message = "AIVDM,1,1,,A,36K2;khOiMawIOvD@78;s9m2010Q,0";
        assert_eq!(compute_checksum(message), 0x2a);

        let message = "AIVDM,1,1,,A,168sg`?0108ch4VEP<N<1Bq005J4,0";
        assert_eq!(compute_checksum(message), 0x20);

        let message = "AIVDM,2,1,1,A,56:=JHP0=T3P0000001TV0P4pN3SSP000000000Q50D5500000000000,0";
        assert_eq!(compute_checksum(message), 0x52);

        let message = "AIVDM,2,1,5,B,59NS6oP2=lu07PDWJ21Tn1=D<<E=>22222222217FiLN:4o:0PTj1Bkm,0";
        assert_eq!(compute_checksum(message), 0x4d);
    }

    #[test]
    fn should_checksum_messages() {
        let message = "AIVDM,2,1,5,B,59NS6oP2=lu07PDWJ21Tn1=D<<E=>22222222217FiLN:4o:0PTj1Bkm,0";
        assert_eq!(checksum(message, 0x4D), true);

        let message = "AIVDM,2,1,5,B,59NS6oP2=lu07PDWJ21Tn1=D<<E=>22222222217FiLN:4o:0PTj1Bkm,0`";
        assert_eq!(checksum(message, 0x4F), false);
    }
}
