use crate::parsers::header::{parser_fragment_header, Header};
use crate::parsers::utils::parsers::complete::hex_to_u32;
use nom::bytes::complete::tag;
use nom::bytes::streaming::take_until;
use nom::character::streaming::digit1;
use nom::combinator::map;
use nom::error::context;
use nom::sequence::tuple;
use nom::Err::Error;
use nom::IResult;

pub fn digit_to_u8(input: &str) -> IResult<&str, u8> {
    context("digit_to_u8", map(digit1, |x: &str| x.parse().unwrap_or(0)))(input)
}

pub fn parse_fragment(message: &str) -> IResult<&str, (Header, &str, u8, u32, &str)> {
    let (remain, input) = take_until("\r\n")(message)?;

    let result = map(
        tuple((
            tag("!"),
            parser_fragment_header,
            parse_ascii,
            tag(","),
            digit_to_u8,
            tag("*"),
            hex_to_u32(2),
        )),
        |(_, header, payload, _, pad, _, checksum)| (header, payload, pad, checksum),
    )(input);

    result
        .map(|(_, (header, payload, pad, checksum))| {
            let remaining_size = remain.len();
            let message_without_checksum = &message[1..message.len() - 3 - remaining_size];

            (
                &remain[2..],
                (header, payload, pad, checksum, message_without_checksum),
            )
        })
        .map_err(|err| match err {
            Error(mut err) => {
                err.input = &remain[2..];
                Error(err)
            }
            e => e,
        })
}

fn parse_ascii(input: &str) -> IResult<&str, &str> {
    nom::bytes::streaming::take_while(|c: char| c.is_ascii() && c as u8 != 0x2a && c as u8 != 0x2c)(
        input,
    )
}

#[cfg(test)]
mod tests {

    use crate::parsers::header::{Channel, Header, Protocol, ProtocolType, TalkerId};
    use crate::parsers::utils::parsers::streaming::{hex_to_u32, parse_ascii, parse_fragment};
    use nom::error::{make_error, ErrorKind};
    use nom::Err::{Error, Incomplete};
    use nom::Needed::Unknown;

    #[test]
    fn should_parse_hex_to_u32() {
        assert_eq!(hex_to_u32(2)("00"), Ok(("", 0)));
        assert_eq!(hex_to_u32(2)("01"), Ok(("", 1)));
        assert_eq!(hex_to_u32(2)("0f"), Ok(("", 0xf)));
        assert_eq!(hex_to_u32(2)("1f"), Ok(("", 0x1f)));
        assert_eq!(hex_to_u32(3)("ED8"), Ok(("", 0xed8)));
        assert_eq!(hex_to_u32(3)("ED8x"), Ok(("x", 0xed8)));
    }

    #[test]
    fn should_parse_fragment() {
        let message =
            "!AIVDM,2,1,5,B,59NS6oP2=lu07PDWJ21Tn1=D<<E=>22222222217FiLN:4o:0PTj1Bkm,0*4D\r\n!E";
        assert_eq!(
            parse_fragment(message),
            Ok((
                "!E",
                (
                    Header {
                        protocol: Protocol {
                            talker_id: TalkerId::AI,
                            protocol_type: ProtocolType::VDM
                        },
                        number_of_fragment: 2,
                        current_fragment: 1,
                        message_id: Some(5),
                        channel: Channel::B
                    },
                    "59NS6oP2=lu07PDWJ21Tn1=D<<E=>22222222217FiLN:4o:0PTj1Bkm",
                    0,
                    0x4D,
                    "AIVDM,2,1,5,B,59NS6oP2=lu07PDWJ21Tn1=D<<E=>22222222217FiLN:4o:0PTj1Bkm,0"
                )
            ))
        );

        let message = "!AIVDM,1,1,,A,15N7th0P00ISsi4A5I?:fgvP2<40,0*06\r\n";
        assert_eq!(
            parse_fragment(message),
            Ok((
                "",
                (
                    Header {
                        protocol: Protocol {
                            talker_id: TalkerId::AI,
                            protocol_type: ProtocolType::VDM
                        },
                        number_of_fragment: 1,
                        current_fragment: 1,
                        message_id: None,
                        channel: Channel::A
                    },
                    "15N7th0P00ISsi4A5I?:fgvP2<40",
                    0,
                    0x06,
                    "AIVDM,1,1,,A,15N7th0P00ISsi4A5I?:fgvP2<40,0"
                )
            ))
        );
    }

    #[test]
    fn should_reject_invalid_message() {
        let message = "!A*3\r\ntoto";
        assert_eq!(
            parse_fragment(message),
            Err(Error(make_error("toto", ErrorKind::Verify)))
        );
    }

    #[test]
    fn should_reject_incomplete_message() {
        let part1 = "!AIVDM,2,2,4";
        assert_eq!(parse_fragment(part1), Err(Incomplete(Unknown)));
    }

    #[test]
    fn should_return_the_remaining_string_in_success_state() {
        let message = "!AIVDM,2,1,9,B,53nFBv01SJ<thHp6220H4heHTf2222222222221?50:454o<`9QSlUDp,0*09\r\n!AIVDM,2,2,9,B,888888888888880,2*2E\r\n";

        assert_eq!(
            parse_fragment(message),
            Ok((
                "!AIVDM,2,2,9,B,888888888888880,2*2E\r\n",
                (
                    Header {
                        protocol: Protocol {
                            talker_id: TalkerId::AI,
                            protocol_type: ProtocolType::VDM
                        },
                        number_of_fragment: 2,
                        current_fragment: 1,
                        message_id: Some(9),
                        channel: Channel::B
                    },
                    "53nFBv01SJ<thHp6220H4heHTf2222222222221?50:454o<`9QSlUDp",
                    0,
                    0x09,
                    "AIVDM,2,1,9,B,53nFBv01SJ<thHp6220H4heHTf2222222222221?50:454o<`9QSlUDp,0"
                )
            ))
        );
    }

    #[test]
    fn should_parse_ascii() {
        assert_eq!(parse_ascii("A😎"), Ok(("😎", "A")));
        assert_eq!(parse_ascii("A,😎"), Ok((",😎", "A")));
        assert_eq!(
            parse_ascii("59NS6oP2=lu07PDWJ21Tn1=D<<E=>😎"),
            Ok(("😎", "59NS6oP2=lu07PDWJ21Tn1=D<<E=>"))
        );
        assert_eq!(
            parse_ascii("59NS6oP2=lu07PDWJ21Tn1=D<<E=>*78"),
            Ok(("*78", "59NS6oP2=lu07PDWJ21Tn1=D<<E=>"))
        );
    }
}
