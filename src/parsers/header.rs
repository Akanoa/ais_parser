use nom::bytes::complete::{tag, take};
use nom::character::complete::{alphanumeric1, digit0};
use nom::combinator::{map, map_res, verify};
use nom::sequence::tuple;
use nom::Err::Error;
use nom::IResult;

use crate::parsers::utils::parsers::complete::digit_to_u8;

#[derive(Eq, PartialEq, Debug)]
#[allow(clippy::upper_case_acronyms)]
pub enum TalkerId {
    AB, // NMEA 4.0 Base AIS station
    AD, // NMEA 4.0 Dependent AIS Base Station
    AI, // Mobile AIS station
    AN, // NMEA 4.0 Aid to Navigation AIS station
    AR, // NMEA 4.0 AIS Receiving Station
    AS, // NMEA 4.0 Limited Base Station
    AT, // NMEA 4.0 AIS Transmitting Station
    AX, // NMEA 4.0 Repeater AIS station
    BS, // Base AIS station (deprecated in NMEA 4.0)
    SA, // NMEA 4.0 Physical Shore AIS Station,
    Unknown,
}

impl TalkerId {
    fn from_string(input: &str) -> TalkerId {
        match input {
            "AB" => TalkerId::AB,
            "AD" => TalkerId::AD,
            "AI" => TalkerId::AI,
            "AN" => TalkerId::AN,
            "AR" => TalkerId::AR,
            "AS" => TalkerId::AS,
            "AT" => TalkerId::AT,
            "AX" => TalkerId::AX,
            "BS" => TalkerId::BS,
            "SA" => TalkerId::SA,
            _ => TalkerId::Unknown,
        }
    }
}

#[derive(Eq, PartialEq, Debug)]
#[allow(clippy::upper_case_acronyms)]
pub enum ProtocolType {
    VDM,
    VDO,
    Unknown,
}

impl ProtocolType {
    fn from_string(input: &str) -> ProtocolType {
        match input {
            "VDM" => ProtocolType::VDM,
            "VDO" => ProtocolType::VDO,
            _ => ProtocolType::Unknown,
        }
    }
}

#[derive(Eq, PartialEq, Debug)]
pub struct Protocol {
    pub(crate) talker_id: TalkerId,
    pub(crate) protocol_type: ProtocolType,
}

/*
 * Is a radio channel code. AIS uses the high side of the duplex from two VHF radio channels:
 *  - AIS Channel A is 161.975Mhz (87B);
 *  - AIS Channel B is 162.025Mhz (88B).
 * In the wild, channel codes '1' and '2' may also be encountered;
 * the standards do not prescribe an interpretation of these but it’s obvious enough.
 */
#[derive(Eq, PartialEq, Debug)]
pub enum Channel {
    A,
    B,
    Unknown,
}

impl Channel {
    fn from_string(input: &str) -> Channel {
        match input {
            "A" | "1" => Channel::A,
            "B" | "2" => Channel::B,
            _ => Channel::Unknown,
        }
    }
}

#[derive(Eq, PartialEq, Debug)]
pub struct Header {
    pub protocol: Protocol,
    pub(crate) number_of_fragment: u8,
    pub(crate) current_fragment: u8,
    pub(crate) message_id: Option<u8>,
    pub(crate) channel: Channel,
}

fn parser_protocol(message: &str) -> IResult<&str, Protocol> {
    fn parser_talker_id(input: &str) -> IResult<&str, TalkerId> {
        let parser = map(take(2u8), TalkerId::from_string);

        verify(parser, |talker_id| *talker_id != TalkerId::Unknown)(input)
    }

    fn parser_protocol_type(input: &str) -> IResult<&str, ProtocolType> {
        let parser = map(take(3u8), ProtocolType::from_string);

        verify(parser, |protocol_type| {
            *protocol_type != ProtocolType::Unknown
        })(input)
    }

    let result = map(
        tuple((parser_talker_id, parser_protocol_type)),
        |(talker_id, protocol_type)| Protocol {
            talker_id,
            protocol_type,
        },
    )(message);

    result.map_err(|err| match err {
        Error(mut err) => {
            err.input = message;
            Error(err)
        }
        error_variant => error_variant,
    })
}

pub fn parser_fragment_header(message: &str) -> IResult<&str, Header> {
    fn parser_message_id(input: &str) -> IResult<&str, Option<u8>> {
        map(digit0, |message_id| match message_id {
            "" => None,
            value => match value.parse::<u8>() {
                Ok(value) => Some(value),
                Err(_) => None,
            },
        })(input)
    }

    fn parser_channel(input: &str) -> IResult<&str, Channel> {
        let parser = map(alphanumeric1, Channel::from_string);

        verify(parser, |channel| *channel != Channel::Unknown)(input)
    }

    let result =
        map_res(
            tuple((
                parser_protocol,
                tag(","),
                digit_to_u8,
                tag(","),
                digit_to_u8,
                tag(","),
                parser_message_id,
                tag(","),
                parser_channel,
                tag(","),
            )),
            |(
                protocol,
                _,
                number_of_fragment,
                _,
                current_fragment,
                _,
                message_id,
                _,
                channel,
                _,
            )|
             -> Result<Header, nom::error::Error<&str>> {
                Ok(Header {
                    protocol,
                    number_of_fragment,
                    current_fragment,
                    message_id,
                    channel,
                })
            },
        )(message);

    result.map_err(|err| match err {
        Error(mut err) => {
            err.input = message;
            Error(err)
        }
        error_variant => error_variant,
    })
}

#[cfg(test)]
mod tests {
    use crate::parsers::header::{
        parser_fragment_header, parser_protocol, Channel, Header, Protocol, ProtocolType, TalkerId,
    };
    use nom::error::{make_error, ErrorKind};
    use nom::Err::Error;

    #[test]
    fn should_parser_protocol_header() {
        let message = "AIVDM,1,1,,B,177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C";
        let expected = Protocol {
            protocol_type: ProtocolType::VDM,
            talker_id: TalkerId::AI,
        };
        assert_eq!(
            parser_protocol(message),
            Ok((",1,1,,B,177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C", expected))
        );

        let message = "AIVDO,1,1,,B,177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C";
        let expected = Protocol {
            protocol_type: ProtocolType::VDO,
            talker_id: TalkerId::AI,
        };
        assert_eq!(
            parser_protocol(message),
            Ok((",1,1,,B,177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C", expected))
        );

        let message = "XIVDO,1,1,,B,177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C";
        assert_eq!(
            parser_protocol(message),
            Err(Error(make_error(
                "XIVDO,1,1,,B,177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C",
                ErrorKind::Verify
            )))
        );

        let message = "AIVDX,1,1,,B,177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C";
        assert_eq!(
            parser_protocol(message),
            Err(Error(make_error(
                "AIVDX,1,1,,B,177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C",
                ErrorKind::Verify
            )))
        );
    }

    #[test]
    fn should_parse_message_header() {
        let message = "AIVDM,1,1,,B,177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C";
        let expected = Header {
            protocol: Protocol {
                protocol_type: ProtocolType::VDM,
                talker_id: TalkerId::AI,
            },
            number_of_fragment: 1u8,
            current_fragment: 1u8,
            message_id: None,
            channel: Channel::B,
        };
        assert_eq!(
            parser_fragment_header(message),
            Ok(("177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C", expected))
        );

        let message = "AIVDM,2,1,5,A,56:;F7P0000088`J2208v0P4V0HDV1`QF222220t32<2G4nU0<V@C`50,0*74";
        let expected = Header {
            protocol: Protocol {
                protocol_type: ProtocolType::VDM,
                talker_id: TalkerId::AI,
            },
            number_of_fragment: 2u8,
            current_fragment: 1u8,
            message_id: Some(5),
            channel: Channel::A,
        };
        assert_eq!(
            parser_fragment_header(message),
            Ok((
                "56:;F7P0000088`J2208v0P4V0HDV1`QF222220t32<2G4nU0<V@C`50,0*74",
                expected
            ))
        );

        let message = "AIVDM,A,1,,B,177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C";
        assert_eq!(
            parser_fragment_header(message),
            Err(Error(make_error(message, ErrorKind::Digit)))
        );

        let message = "AIVDM,1,1,,9,177KQJ5000G?tO`K>RA1wUbN0TKH,0*5C";
        assert_eq!(
            parser_fragment_header(message),
            Err(Error(make_error(message, ErrorKind::Verify)))
        );
    }
}
