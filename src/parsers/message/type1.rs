use crate::parsers::utils::data::complement_2_u8;
use crate::parsers::utils::parsers::complete::{
    parser_bool_field, parser_i32_field, parser_u16_field, parser_u32_field, parser_u8_field,
};
use nom::bytes::complete::take;
use nom::combinator::map;
use nom::sequence::tuple;
use nom::IResult;
use serde::{Deserialize, Serialize};
use std::convert::TryInto;
use std::fmt::{Display, Formatter};

#[derive(PartialOrd, PartialEq, Debug, Clone, Serialize, Deserialize)]
enum NavigationStatus {
    UnderWayUsingEngine,
    AtAnchor,
    NotUnderCommand,
    RestrictedManoeuvrability,
    ConstrainedByHerDraught,
    Moored,
    AGround,
    EngagedInFishing,
    UnderWaySailing,
    AisSartActivated,
    Default,
}

impl NavigationStatus {
    fn from_u8(code: u8) -> Self {
        match code {
            0 => NavigationStatus::UnderWayUsingEngine,
            1 => NavigationStatus::AtAnchor,
            2 => NavigationStatus::NotUnderCommand,
            3 => NavigationStatus::RestrictedManoeuvrability,
            4 => NavigationStatus::ConstrainedByHerDraught,
            5 => NavigationStatus::Moored,
            6 => NavigationStatus::AGround,
            7 => NavigationStatus::EngagedInFishing,
            8 => NavigationStatus::UnderWaySailing,
            14 => NavigationStatus::AisSartActivated,
            _ => NavigationStatus::Default,
        }
    }
}

#[derive(PartialOrd, PartialEq, Debug, Clone, Serialize, Deserialize)]
enum TurningRateStatus {
    NotTurning,
    TurningLeft(u16),
    TurningRight(u16),
    TurningLeftExtra,
    TurningRightExtra,
    NotAvailable,
}

/*
 *  Turn rate is encoded as follows:
 *
 *   - 0        = not turning
 *   - 1..126   = turning right at up to 708 degrees per minute or higher
 *   - -1..-126  = turning left at up to 708 degrees per minute or higher
 *   - 127      = turning right at more than 5deg/30s (No TI available)
 *   - -127     = turning left at more than 5deg/30s (No TI available)
 *   - -128     = indicates no turn information available (default)
 *
 *  Values between 0 and 708 degrees/min coded by ROTAIS=4.733 * SQRT(ROTsensor) degrees/min where ROTsensor is
 *  the Rate of Turn as input by an external Rate of Turn Indicator. ROTAIS is rounded to the nearest integer value.
 *  Thus, to decode the field value, divide by 4.733 and then square it.
 *  Sign of the field value should be preserved when squaring it, otherwise the left/right indication will be lost.
 */
impl TurningRateStatus {
    fn from_i8(value: i8) -> TurningRateStatus {
        fn to_degree_per_min(value: u8) -> u16 {
            (value as f32 / 4.733).powi(2) as u16
        }

        match value {
            0 => TurningRateStatus::NotTurning,
            x if (1..=126).contains(&x) => {
                TurningRateStatus::TurningRight(to_degree_per_min(value as u8))
            }
            x if (-126..=-1).contains(&x) => {
                TurningRateStatus::TurningLeft(to_degree_per_min(-value as u8))
            }
            127 => TurningRateStatus::TurningRightExtra,
            -127 => TurningRateStatus::TurningLeftExtra,
            _ => TurningRateStatus::NotAvailable,
        }
    }
}

#[derive(PartialOrd, PartialEq, Debug, Clone, Serialize, Deserialize)]
enum SpeedOverGround {
    Value(f32),
    Greater,
    NotAvailable,
}

/*
 * Speed over ground is in 0.1-knot resolution from 0 to 102 knots.
 * Value 1023 indicates speed is not available, value 1022 indicates 102.2 knots or higher.
 */
impl SpeedOverGround {
    fn from_u16(value: u16) -> SpeedOverGround {
        match value {
            1022 => SpeedOverGround::Greater,
            1023 => SpeedOverGround::NotAvailable,
            x => SpeedOverGround::Value(x as f32 / 10f32),
        }
    }
}

#[derive(PartialOrd, PartialEq, Debug, Clone, Serialize, Deserialize)]
enum Longitude {
    East(f32),
    West(f32),
    NotAvailable,
}

fn to_degree(value: i32) -> f32 {
    value as f32 / 600000.0
}

/*
 * Longitude is given in in 1/10000 min; divide by 600000.0 to obtain degrees.
 * Values up to plus or minus 180 degrees, East = positive, West \= negative.
 * A value of 181 degrees (0x6791AC0 hex) indicates that longitude is not available and is the default.
 */
impl Longitude {
    fn from_i32(value: i32) -> Longitude {
        match value {
            0x6791AC0 => Longitude::NotAvailable,
            value if value > 0 => Longitude::East(to_degree(value)),
            value => Longitude::West(-to_degree(value)),
        }
    }
}

impl TryInto<f32> for Longitude {
    type Error = ();

    fn try_into(self) -> Result<f32, Self::Error> {
        match &self {
            Longitude::East(value) => Ok(*value),
            Longitude::West(value) => Ok(-(*value)),
            Longitude::NotAvailable => Err(()),
        }
    }
}

#[derive(PartialOrd, PartialEq, Debug, Clone, Serialize, Deserialize)]
enum Latitude {
    North(f32),
    South(f32),
    NotAvailable,
}

/*
 * Latitude is given in in 1/10000 min; divide by 600000.0 to obtain degrees.
 * Values up to plus or minus 90 degrees, North = positive, South = negative.
 * A value of 91 degrees (0x3412140 hex) indicates latitude is not available and is the default.
 */
impl Latitude {
    fn from_i32(value: i32) -> Latitude {
        match value {
            0x3412140 => Latitude::NotAvailable,
            x if x >= 0 => Latitude::North(to_degree(x)),
            x => Latitude::South(-to_degree(x)),
        }
    }
}

impl TryInto<f32> for Latitude {
    type Error = ();

    fn try_into(self) -> Result<f32, Self::Error> {
        match &self {
            Latitude::North(value) => Ok(*value),
            Latitude::South(value) => Ok(-(*value)),
            Latitude::NotAvailable => Err(()),
        }
    }
}

struct CourseOverGround;
/*
 * Course over ground will be 3600 (0xE10) if that data is not available.
 * Else COG is value / 10
 */
impl CourseOverGround {
    fn from_u16(value: u16) -> Option<f32> {
        match value {
            0xe10 => None,
            x => Some((x as f32) / 10f32),
        }
    }
}

struct Heading;
/*
 * 0 to 359 degrees, 511 = not available.
 */
impl Heading {
    fn from_u16(value: u16) -> Option<u16> {
        match value {
            x if x <= 359 => Some(x),
            _ => None,
        }
    }
}

#[derive(PartialOrd, PartialEq, Debug, Clone, Serialize, Deserialize)]
enum TimeStamp {
    Value(u8),
    ManualMode,
    EstimatedMode,
    Inoperative,
    NotAvailable,
}

/*
 * Seconds in UTC timestamp should be 0-59, except for these special values:
 *
 * 60 if time stamp is not available (default)
 * 61 if positioning system is in manual input mode
 * 62 if Electronic Position Fixing System operates in estimated (dead reckoning) mode,
 * 63 if the positioning system is inoperative.
 */
impl TimeStamp {
    fn from_u8(value: u8) -> TimeStamp {
        match value {
            60 => TimeStamp::NotAvailable,
            61 => TimeStamp::ManualMode,
            62 => TimeStamp::EstimatedMode,
            63 => TimeStamp::Inoperative,
            x => TimeStamp::Value(x),
        }
    }
}

#[derive(PartialOrd, PartialEq, Debug, Clone, Serialize, Deserialize)]
enum ManeuverIndicator {
    NoSpecial,
    Special,
    NotAvailable,
}

impl ManeuverIndicator {
    fn from_u8(value: u8) -> ManeuverIndicator {
        match value {
            1 => ManeuverIndicator::NoSpecial,
            2 => ManeuverIndicator::Special,
            _ => ManeuverIndicator::NotAvailable,
        }
    }
}

#[derive(PartialOrd, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct MessageType1 {
    message_type: u8,
    repeat: u8,
    pub maritime_mobile_service_identity: u32,
    navigation_status: NavigationStatus,
    turning_rate: TurningRateStatus,
    speed_over_ground: SpeedOverGround,
    position_accuracy: bool,
    longitude: Longitude,
    latitude: Latitude,
    pub course_over_ground: Option<f32>,
    pub heading: Option<u16>,
    second_of_timestamp: TimeStamp,
    maneuver_indicator: ManeuverIndicator,
    receiver_autonomous_integrity_monitoring: bool,
    radio_status: u32,
}

impl Display for MessageType1 {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}

fn parse_navigation_status(payload: &str) -> IResult<&str, NavigationStatus> {
    map(parser_u8_field(4usize), NavigationStatus::from_u8)(payload)
}

fn parse_turning_rate_status(payload: &str) -> IResult<&str, TurningRateStatus> {
    map(parser_u8_field(8usize), |status| {
        TurningRateStatus::from_i8(complement_2_u8(status, 8))
    })(payload)
}

fn parse_speed_over_ground(payload: &str) -> IResult<&str, SpeedOverGround> {
    map(parser_u16_field(10), SpeedOverGround::from_u16)(payload)
}

fn parse_longitude(payload: &str) -> IResult<&str, Longitude> {
    map(parser_i32_field(28), Longitude::from_i32)(payload)
}

fn parse_latitude(payload: &str) -> IResult<&str, Latitude> {
    map(parser_i32_field(27), Latitude::from_i32)(payload)
}

fn parse_course_over_ground(payload: &str) -> IResult<&str, Option<f32>> {
    map(parser_u16_field(12), CourseOverGround::from_u16)(payload)
}

fn parse_heading(payload: &str) -> IResult<&str, Option<u16>> {
    map(parser_u16_field(9), Heading::from_u16)(payload)
}

fn parse_timestamp(payload: &str) -> IResult<&str, TimeStamp> {
    map(parser_u8_field(6), TimeStamp::from_u8)(payload)
}

fn parse_maneuver_indicator(payload: &str) -> IResult<&str, ManeuverIndicator> {
    map(parser_u8_field(2), ManeuverIndicator::from_u8)(payload)
}

pub fn parser_payload_type1(payload: &str) -> IResult<&str, MessageType1> {
    let (
        input,
        (
            message_type,
            repeat,
            maritime_mobile_service_identity,
            navigation_status,
            turning_rate,
            speed_over_ground,
            position_accuracy,
            longitude,
            latitude,
            course_over_ground,
            heading,
            second_of_timestamp,
            maneuver_indicator,
            _,
            receiver_autonomous_integrity_monitoring,
            radio_status,
        ),
    ) = tuple((
        parser_u8_field(6usize),
        parser_u8_field(2usize),
        parser_u32_field(30usize),
        parse_navigation_status,
        parse_turning_rate_status,
        parse_speed_over_ground,
        parser_bool_field,
        parse_longitude,
        parse_latitude,
        parse_course_over_ground,
        parse_heading,
        parse_timestamp,
        parse_maneuver_indicator,
        take(3usize),
        parser_bool_field,
        parser_u32_field(19usize),
    ))(payload)?;

    Ok((
        input,
        MessageType1 {
            message_type,
            repeat,
            maritime_mobile_service_identity,
            navigation_status,
            turning_rate,
            speed_over_ground,
            position_accuracy,
            longitude,
            latitude,
            course_over_ground,
            heading,
            second_of_timestamp,
            maneuver_indicator,
            receiver_autonomous_integrity_monitoring,
            radio_status,
        },
    ))
}

impl MessageType1 {
    /// Return a tuple(longitude, latitude) of ship coordinates
    pub fn get_coordinates(&self) -> (f32, f32) {
        let mut result = (0f32, 0f32);

        if let Ok(value) = self.clone().longitude.try_into() {
            result.0 = value;
        } else {
            return (0f32, 0f32);
        }

        if let Ok(value) = self.clone().latitude.try_into() {
            result.1 = value;
        } else {
            return (0f32, 0f32);
        }

        result
    }
}

#[cfg(test)]
mod tests {
    use crate::parsers::message::type1::{
        parse_course_over_ground, parse_heading, parse_latitude, parse_longitude,
        parse_maneuver_indicator, parse_navigation_status, parse_speed_over_ground,
        parse_timestamp, parse_turning_rate_status, parser_payload_type1, Latitude, Longitude,
        ManeuverIndicator, MessageType1, NavigationStatus, SpeedOverGround, TimeStamp,
        TurningRateStatus,
    };
    use crate::parsers::utils::data::uncompress_payload;
    use float_cmp::{approx_eq, ApproxEq, F32Margin};

    // 000001 00 011100011101101110000101101000 0101 00000000 0000000000 0 1011100111111110001111110100 0011011001110100010010001000001111111100101101010011110000000100100011011011000

    #[test]
    fn should_parse_navigation_status() {
        fn create_payload(navigation_status_code: u8) -> String {
            format!(
                "{}{}{}{:04b}{}{}{}{}{}{}{}{}{}",
                "000001",
                "00",
                "011100011101101110000101101000",
                navigation_status_code,
                "00000000",
                "0000000000",
                "0",
                "0101110011111111000111111011",
                "010111001111111100011111101",
                "100111111110",
                "100111111",
                "100111",
                "000001111111100101101010011110000000100100011011011000"
            )
        }

        let payload = &uncompress_payload("177KQJ5000G?tO`K>RA1wUbN0TKH").unwrap()[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::Moored))
        );

        let payload = &create_payload(0)[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::UnderWayUsingEngine))
        );

        let payload = &create_payload(1)[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::AtAnchor))
        );

        let payload = &create_payload(2)[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::NotUnderCommand))
        );

        let payload = &create_payload(3)[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::RestrictedManoeuvrability))
        );

        let payload = &create_payload(4)[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::ConstrainedByHerDraught))
        );

        let payload = &create_payload(5)[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::Moored))
        );

        let payload = &create_payload(6)[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::AGround))
        );

        let payload = &create_payload(7)[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::EngagedInFishing))
        );

        let payload = &create_payload(8)[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::UnderWaySailing))
        );

        let payload = &create_payload(14)[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::AisSartActivated))
        );

        let payload = &create_payload(15)[..];
        assert_eq!(
            parse_navigation_status(&payload[38..]),
            Ok((&payload[42..], NavigationStatus::Default))
        );
    }

    #[test]
    fn should_parse_turning_rate_status() {
        fn create_payload(turning_value: &str) -> String {
            format!("{}{}{}{}{}{}", "000001", "00", "011100011101101110000101101000", "0101", turning_value, "0000000000010111001111111100011111101000011011001110100010010001000001111111100101101010011110000000100100011011011000")
        }

        let payload = &create_payload("00000000")[..];
        assert_eq!(
            parse_turning_rate_status(&payload[42..]),
            Ok((&payload[50..], TurningRateStatus::NotTurning))
        );

        let payload = &create_payload("00001111")[..];
        assert_eq!(
            parse_turning_rate_status(&payload[42..]),
            Ok((&payload[50..], TurningRateStatus::TurningRight(10u16)))
        );

        let payload = &create_payload("11110001")[..];
        assert_eq!(
            parse_turning_rate_status(&payload[42..]),
            Ok((&payload[50..], TurningRateStatus::TurningLeft(10u16)))
        );

        let payload = &create_payload("01111111")[..];
        assert_eq!(
            parse_turning_rate_status(&payload[42..]),
            Ok((&payload[50..], TurningRateStatus::TurningRightExtra))
        );

        let payload = &create_payload("10000001")[..];
        assert_eq!(
            parse_turning_rate_status(&payload[42..]),
            Ok((&payload[50..], TurningRateStatus::TurningLeftExtra))
        );

        let payload = &create_payload("10000000")[..];
        assert_eq!(
            parse_turning_rate_status(&payload[42..]),
            Ok((&payload[50..], TurningRateStatus::NotAvailable))
        );
    }

    #[test]
    fn should_parse_speed_over_ground() {
        fn create_payload(speed_over_ground: u16) -> String {
            format!("{}{}{}{}{}{:010b}{}", "000001", "00", "011100011101101110000101101000", "0101", "00000000", speed_over_ground, "010111001111111100011111101000011011001110100010010001000001111111100101101010011110000000100100011011011000")
        }

        let payload = &create_payload(0)[..];
        assert_eq!(
            parse_speed_over_ground(&payload[50..]),
            Ok((&payload[60..], SpeedOverGround::Value(0f32)))
        );

        let payload = &create_payload(152)[..];
        assert_eq!(
            parse_speed_over_ground(&payload[50..]),
            Ok((&payload[60..], SpeedOverGround::Value(15.2)))
        );

        let payload = &create_payload(1022)[..];
        assert_eq!(
            parse_speed_over_ground(&payload[50..]),
            Ok((&payload[60..], SpeedOverGround::Greater))
        );

        let payload = &create_payload(1023)[..];
        assert_eq!(
            parse_speed_over_ground(&payload[50..]),
            Ok((&payload[60..], SpeedOverGround::NotAvailable))
        );
    }

    #[test]
    fn should_parse_longitude() {
        fn create_payload(longitude: f32) -> String {
            let longitude_time_factor = (longitude * 600000.0) as i32;
            let longitude_complement_2 = match longitude_time_factor {
                x if x >= 0 => x as u32,
                a => ((!((a.abs() as u32) << 4)).wrapping_add(1)) >> 4,
            };

            format!("{}{}{}{}{}{}{}{:028b}{}",
                    "000001",
                    "00",
                    "011100011101101110000101101000",
                    "0101",
                    "00000000",
                    "0000000000",
                    "0",
                    longitude_complement_2,
                    "010111001111111100011111101000011011001110100010010001000001111111100101101010011110000000100100011011011000")
        }

        let payload = &create_payload(45.5f32)[..];
        assert_eq!(
            parse_longitude(&payload[61..]),
            Ok((&payload[89..], Longitude::East(45.5f32)))
        );

        let payload = &create_payload(179.0153792f32)[..];
        assert_eq!(
            parse_longitude(&payload[61..]),
            Ok((&payload[89..], Longitude::East(179.0153792f32)))
        );

        let payload = &create_payload(-45.5f32)[..];
        assert_eq!(
            parse_longitude(&payload[61..]),
            Ok((&payload[89..], Longitude::West(45.5f32)))
        );

        let payload = &create_payload(-45.558954f32)[..];
        assert_eq!(
            parse_longitude(&payload[61..]),
            Ok((&payload[89..], Longitude::West(45.558954f32)))
        );

        let payload = &create_payload(181.0 as f32)[..];
        assert_eq!(
            parse_longitude(&payload[61..]),
            Ok((&payload[89..], Longitude::NotAvailable))
        );
    }

    #[test]
    fn should_parse_latitude() {
        fn create_payload(latitude: f32) -> String {
            let latitude_time_factor = (latitude * 600000.0) as i32;
            let latitude_complement_2 = match latitude_time_factor {
                x if x >= 0 => x as u32,
                a => ((!((a.abs() as u32) << 5)).wrapping_add(1)) >> 5,
            };

            format!(
                "{}{}{}{}{}{}{}{}{:027b}{}",
                "000001",
                "00",
                "011100011101101110000101101000",
                "0101",
                "00000000",
                "0000000000",
                "0",
                "0101110011111111000111111011",
                latitude_complement_2,
                "000011011001110100010010001000001111111100101101010011110000000100100011011011000"
            )
        }

        let payload = &create_payload(45.5f32)[..];
        assert_eq!(
            parse_latitude(&payload[89..]),
            Ok((&payload[116..], Latitude::North(45.5f32)))
        );

        let payload = &create_payload(89.0153792f32)[..];
        assert_eq!(
            parse_latitude(&payload[89..]),
            Ok((&payload[116..], Latitude::North(89.0153792f32)))
        );

        let payload = &create_payload(-45.5f32)[..];
        assert_eq!(
            parse_latitude(&payload[89..]),
            Ok((&payload[116..], Latitude::South(45.5f32)))
        );

        let payload = &create_payload(-45.558954f32)[..];
        assert_eq!(
            parse_latitude(&payload[89..]),
            Ok((&payload[116..], Latitude::South(45.558954f32)))
        );

        let payload = &create_payload(91.0 as f32)[..];
        assert_eq!(
            parse_latitude(&payload[89..]),
            Ok((&payload[116..], Latitude::NotAvailable))
        );
    }

    #[test]
    fn should_parse_course_over_ground() {
        fn create_payload(cog: f32) -> String {
            format!(
                "{}{}{}{}{}{}{}{}{}{:012b}{}",
                "000001",
                "00",
                "011100011101101110000101101000",
                "0101",
                "00000000",
                "0000000000",
                "0",
                "0101110011111111000111111011",
                "010111001111111100011111101",
                (cog * 10f32) as u16,
                "110100010010001000001111111100101101010011110000000100100011011011000"
            )
        }

        let payload = &create_payload(12.5)[..];
        assert_eq!(
            parse_course_over_ground(&payload[116..]),
            Ok((&payload[128..], Some(12.5)))
        );

        let payload = &create_payload(48.33)[..];
        assert_eq!(
            parse_course_over_ground(&payload[116..]),
            Ok((&payload[128..], Some(48.3)))
        );

        let payload = &create_payload(360.0)[..];
        assert_eq!(
            parse_course_over_ground(&payload[116..]),
            Ok((&payload[128..], None))
        );
    }

    #[test]
    fn should_parse_heading() {
        fn create_payload(heading: u16) -> String {
            format!(
                "{}{}{}{}{}{}{}{}{}{}{:09b}{}",
                "000001",
                "00",
                "011100011101101110000101101000",
                "0101",
                "00000000",
                "0000000000",
                "0",
                "0101110011111111000111111011",
                "010111001111111100011111101",
                "100111111110",
                heading,
                "010001000001111111100101101010011110000000100100011011011000"
            )
        }

        let payload = &create_payload(77)[..];
        assert_eq!(
            parse_heading(&payload[128..]),
            Ok((&payload[137..], Some(77)))
        );

        let payload = &create_payload(0)[..];
        assert_eq!(
            parse_heading(&payload[128..]),
            Ok((&payload[137..], Some(0)))
        );

        let payload = &create_payload(359)[..];
        assert_eq!(
            parse_heading(&payload[128..]),
            Ok((&payload[137..], Some(359)))
        );

        let payload = &create_payload(511)[..];
        assert_eq!(parse_heading(&payload[128..]), Ok((&payload[137..], None)));

        let payload = &create_payload(360)[..];
        assert_eq!(parse_heading(&payload[128..]), Ok((&payload[137..], None)));
    }

    #[test]
    fn should_parse_timestamp() {
        fn create_payload(second: u8) -> String {
            format!(
                "{}{}{}{}{}{}{}{}{}{}{}{:06b}{}",
                "000001",
                "00",
                "011100011101101110000101101000",
                "0101",
                "00000000",
                "0000000000",
                "0",
                "0101110011111111000111111011",
                "010111001111111100011111101",
                "100111111110",
                "100111111",
                second,
                "000001111111100101101010011110000000100100011011011000"
            )
        }

        let payload = &create_payload(0)[..];
        assert_eq!(
            parse_timestamp(&payload[137..]),
            Ok((&payload[143..], TimeStamp::Value(0)))
        );

        let payload = &create_payload(59)[..];
        assert_eq!(
            parse_timestamp(&payload[137..]),
            Ok((&payload[143..], TimeStamp::Value(59)))
        );

        let payload = &create_payload(7)[..];
        assert_eq!(
            parse_timestamp(&payload[137..]),
            Ok((&payload[143..], TimeStamp::Value(7)))
        );

        let payload = &create_payload(60)[..];
        assert_eq!(
            parse_timestamp(&payload[137..]),
            Ok((&payload[143..], TimeStamp::NotAvailable))
        );

        let payload = &create_payload(61)[..];
        assert_eq!(
            parse_timestamp(&payload[137..]),
            Ok((&payload[143..], TimeStamp::ManualMode))
        );

        let payload = &create_payload(62)[..];
        assert_eq!(
            parse_timestamp(&payload[137..]),
            Ok((&payload[143..], TimeStamp::EstimatedMode))
        );

        let payload = &create_payload(63)[..];
        assert_eq!(
            parse_timestamp(&payload[137..]),
            Ok((&payload[143..], TimeStamp::Inoperative))
        );
    }

    #[test]
    fn should_parse_maneuver_indicator() {
        fn create_payload(maneuver_indicator: u8) -> String {
            format!(
                "{}{}{}{}{}{}{}{}{}{}{}{}{:02b}{}",
                "000001",
                "00",
                "011100011101101110000101101000",
                "0101",
                "00000000",
                "0000000000",
                "0",
                "0101110011111111000111111011",
                "010111001111111100011111101",
                "100111111110",
                "100111111",
                "100111",
                maneuver_indicator,
                "0001111111100101101010011110000000100100011011011000"
            )
        }

        let payload = &create_payload(0)[..];
        assert_eq!(
            parse_maneuver_indicator(&payload[143..]),
            Ok((&payload[145..], ManeuverIndicator::NotAvailable))
        );

        let payload = &create_payload(1)[..];
        assert_eq!(
            parse_maneuver_indicator(&payload[143..]),
            Ok((&payload[145..], ManeuverIndicator::NoSpecial))
        );

        let payload = &create_payload(2)[..];
        assert_eq!(
            parse_maneuver_indicator(&payload[143..]),
            Ok((&payload[145..], ManeuverIndicator::Special))
        );
    }

    #[test]
    fn should_parse_message_type_1() {
        let payload = &uncompress_payload("14VE:8002`9hn`1qmIe3Pjrj00SF").unwrap()[..];
        assert_eq!(
            parser_payload_type1(payload),
            Ok((
                "",
                MessageType1 {
                    message_type: 1,
                    repeat: 0,
                    maritime_mobile_service_identity: 308628000,
                    navigation_status: NavigationStatus::UnderWayUsingEngine,
                    turning_rate: TurningRateStatus::NotTurning,
                    speed_over_ground: SpeedOverGround::Value(16.8),
                    position_accuracy: false,
                    longitude: Longitude::East(136.50133),
                    latitude: Latitude::South(10.7751665),
                    course_over_ground: Some(89.9),
                    heading: Some(93),
                    second_of_timestamp: TimeStamp::Value(25),
                    maneuver_indicator: ManeuverIndicator::NotAvailable,
                    receiver_autonomous_integrity_monitoring: false,
                    radio_status: 2262
                }
            ))
        );
    }

    #[derive(Debug)]
    struct Wrapper((f32, f32));

    impl ApproxEq for Wrapper {
        type Margin = F32Margin;

        fn approx_eq<T: Into<Self::Margin>>(self, other: Self, margin: T) -> bool {
            let margin = margin.into();

            self.0 .0.approx_eq(other.0 .0, margin) && self.0 .1.approx_eq(other.0 .1, margin)
        }
    }

    fn create_message_with_coordinates(lon: f32, lat: f32) -> MessageType1 {
        let longitude = if lon < 0 as f32 {
            Longitude::West(-lon)
        } else {
            Longitude::East(lon)
        };

        let latitude = if lat < 0 as f32 {
            Latitude::South(-lat)
        } else {
            Latitude::North(lat)
        };

        MessageType1 {
            message_type: 1,
            repeat: 0,
            maritime_mobile_service_identity: 308628000,
            navigation_status: NavigationStatus::UnderWayUsingEngine,
            turning_rate: TurningRateStatus::NotTurning,
            speed_over_ground: SpeedOverGround::Value(16.8),
            position_accuracy: false,
            longitude,
            latitude,
            course_over_ground: Some(89.9),
            heading: Some(93),
            second_of_timestamp: TimeStamp::Value(25),
            maneuver_indicator: ManeuverIndicator::NotAvailable,
            receiver_autonomous_integrity_monitoring: false,
            radio_status: 2262,
        }
    }

    fn create_message_with_lat_lon(longitude: Longitude, latitude: Latitude) -> MessageType1 {
        MessageType1 {
            message_type: 1,
            repeat: 0,
            maritime_mobile_service_identity: 308628000,
            navigation_status: NavigationStatus::UnderWayUsingEngine,
            turning_rate: TurningRateStatus::NotTurning,
            speed_over_ground: SpeedOverGround::Value(16.8),
            position_accuracy: false,
            longitude,
            latitude,
            course_over_ground: Some(89.9),
            heading: Some(93),
            second_of_timestamp: TimeStamp::Value(25),
            maneuver_indicator: ManeuverIndicator::NotAvailable,
            receiver_autonomous_integrity_monitoring: false,
            radio_status: 2262,
        }
    }

    #[test]
    fn should_return_valid_coordinates_longitude_positive_latitude_negative() {
        let result = (136.50133, -10.7751665);
        let message = create_message_with_coordinates(result.0, result.1);
        let a = Wrapper(message.get_coordinates());
        let b = Wrapper(result);

        assert!(approx_eq!(Wrapper, a, b));
    }

    #[test]
    fn should_return_valid_coordinates_longitude_positive_latitude_positive() {
        let result = (136.50133, 0.7751665);
        let message = create_message_with_coordinates(result.0, result.1);
        let a = Wrapper(message.get_coordinates());
        let b = Wrapper(result);

        assert!(approx_eq!(Wrapper, a, b));
    }

    #[test]
    fn should_return_valid_coordinates_longitude_negative_latitude_positive() {
        let result = (-136.50133, 0.7751665);
        let message = create_message_with_coordinates(result.0, result.1);
        let a = Wrapper(message.get_coordinates());
        let b = Wrapper(result);

        assert!(approx_eq!(Wrapper, a, b));
    }

    #[test]
    fn should_return_valid_coordinates_longitude_negative_latitude_negative() {
        let result = (-136.50133, -10.7751665);
        let message = create_message_with_coordinates(result.0, result.1);
        let a = Wrapper(message.get_coordinates());
        let b = Wrapper(result);

        assert!(approx_eq!(Wrapper, a, b));
    }

    #[test]
    fn should_return_invalid_coordinates() {
        let result = (0.0, 0.0);
        let message = create_message_with_lat_lon(Longitude::NotAvailable, Latitude::NotAvailable);

        let a = Wrapper(message.get_coordinates());
        let b = Wrapper(result);
        assert!(approx_eq!(Wrapper, a, b));

        let result = (0.0, 0.0);
        let message = create_message_with_lat_lon(Longitude::West(1.0), Latitude::NotAvailable);

        let a = Wrapper(message.get_coordinates());
        let b = Wrapper(result);
        assert!(approx_eq!(Wrapper, a, b));

        let result = (0.0, 0.0);
        let message = create_message_with_lat_lon(Longitude::NotAvailable, Latitude::North(1.0));

        let a = Wrapper(message.get_coordinates());
        let b = Wrapper(result);
        assert!(approx_eq!(Wrapper, a, b));
    }
}
