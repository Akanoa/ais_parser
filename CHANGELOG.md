# [1.3.0](https://gitlab.com/projets-akanoa/pipelines/pipelines-plugin-ais/compare/v1.2.0...v1.3.0) (2022-04-10)


### Bug Fixes

* Handle never completed frames ([9f321ba](https://gitlab.com/projets-akanoa/pipelines/pipelines-plugin-ais/commit/9f321ba3310d7752c328a7d0708c8536946b3474))
* Make ais_parser structure compatible with stream duplicator ([ac53620](https://gitlab.com/projets-akanoa/pipelines/pipelines-plugin-ais/commit/ac536205450061acfc3a767728755aabb908cdc1))


### Features

* Allow to get Ship coordinates ([1a76666](https://gitlab.com/projets-akanoa/pipelines/pipelines-plugin-ais/commit/1a76666ab00e6d3cc588b0d5cf6185731724ba8c))
* Open COG field ([3fb19d5](https://gitlab.com/projets-akanoa/pipelines/pipelines-plugin-ais/commit/3fb19d5d0b6a6aab270217bdcb3f2933894fc247))
* Open ship geometry properties ([93ed650](https://gitlab.com/projets-akanoa/pipelines/pipelines-plugin-ais/commit/93ed650b7a6517b74531c08b630a890ce8ac2ae2))
* Open ship_name field ([eeac259](https://gitlab.com/projets-akanoa/pipelines/pipelines-plugin-ais/commit/eeac2593e5a8a2baab153c5a5ccbd0f36251fc7c))

# [1.2.0](https://gitlab.com/Akanoa/ais_parser/compare/v1.1.0...v1.2.0) (2022-03-01)


### Features

* Allow to parse type5 messages ([ea24801](https://gitlab.com/Akanoa/ais_parser/commit/ea2480112fff09e81a4eada0fd5a248bfd34cade))

# [1.1.0](https://gitlab.com/Akanoa/ais_parser/compare/v1.0.1...v1.1.0) (2020-11-19)


### Features

* Permettre de parser des messages incomplet ou fragmenté ([5b5b72d](https://gitlab.com/Akanoa/ais_parser/commit/5b5b72d905fe49e91b379cfd94d112ed288e245b))

## [1.0.1](https://gitlab.com/Akanoa/ais_parser/compare/v1.0.0...v1.0.1) (2020-11-14)


### Bug Fixes

* Add Changelog file ([b4bf432](https://gitlab.com/Akanoa/ais_parser/commit/b4bf432e8b7b4264f7ddd077c07346cdf171c9f1))
