use nom::bytes::complete::take;
use nom::combinator::{all_consuming, map, map_res, verify};
use nom::multi::many0;
use nom::IResult;

/*
 1 - convert string slice into u8
 2 - check whether this value is defined in six_bit char specification, lower than 47
 3 - if value < 32 => value + 64
     else value
 4 - get ascii char from value
*/
fn convert_six_bit_as_ascii_char(six_bit: &str) -> Option<char> {
    match u8::from_str_radix(six_bit, 2) {
        Ok(code) if code > 63 => None,
        Ok(code) if code < 32 => Some((code + 64) as char),
        Ok(code) => Some(code as char),
        Err(_e) => None,
    }
}

fn parse_six_bits(input: &str) -> IResult<&str, char> {
    let parser = map(take(6usize), convert_six_bit_as_ascii_char);

    map(verify(parser, |option| *option != None), |ok| ok.unwrap())(input)
}

fn cleanup_garbage(chars: Vec<char>) -> String {
    chars.into_iter().filter(|&x| x != '@').collect()
}

pub fn parse_as_string(len: usize) -> impl Fn(&str) -> IResult<&str, String> {
    move |input: &str| {
        let parser = map_res(take(len), all_consuming(many0(parse_six_bits)));

        map(parser, |(_, chars)| {
            cleanup_garbage(chars).trim().to_string()
        })(input)
    }
}

#[cfg(test)]
mod tests {
    use crate::parsers::string::{convert_six_bit_as_ascii_char, parse_as_string};
    use nom::error::{ErrorKind, ParseError};
    use nom::Err;

    #[test]
    fn should_convert_bunch_of_six_bit_as_string() {
        let payload = "000011001000000001010100";
        let result = Ok(("", "CHAT".to_string()));
        assert_eq!(parse_as_string(24)(payload), result);

        let payload = "0000110010000a00001010100";
        let result = Ok(("0010000a00001010100", "C".to_string()));
        assert_eq!(parse_as_string(6)(payload), result);

        let payload = "0000110010000a00001010100";
        let result = Err(Err::Error(ParseError::from_error_kind(
            "0000110010000a00001010100",
            ErrorKind::MapRes,
        )));
        assert_eq!(parse_as_string(25)(payload), result);

        let payload = "000011110110000110001110110110100000100000";
        let result = Ok(("", "C6FN6".to_string()));
        assert_eq!(parse_as_string(42)(payload), result);

        let payload = "001110001101000101000001010011001001001101000000000000000000000000000000000000000000000000000000000000000000000000000000";
        let result = Ok(("", "NMEASIM".to_string()));
        assert_eq!(parse_as_string(120)(payload), result);
    }

    #[test]
    fn should_convert_six_bit_as_ascii_char() {
        assert_eq!(convert_six_bit_as_ascii_char("000000"), Some('@'));
        assert_eq!(convert_six_bit_as_ascii_char("011111"), Some('_'));
        assert_eq!(convert_six_bit_as_ascii_char("100000"), Some(' '));
        assert_eq!(convert_six_bit_as_ascii_char("110000"), Some('0'));
        assert_eq!(convert_six_bit_as_ascii_char("111111"), Some('?'));
        assert_eq!(convert_six_bit_as_ascii_char("1111111"), None);
        assert_eq!(convert_six_bit_as_ascii_char("toto"), None);
    }
}
