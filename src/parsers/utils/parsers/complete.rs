use crate::parsers::utils::data::complement_2_u32;
use nom::branch::alt;
use nom::bytes::complete::{tag, take};
use nom::character::complete::digit1;
use nom::combinator::{map, map_opt, map_res, peek};
use nom::error::{make_error, ErrorKind};
use nom::Err::Error;
use nom::IResult;

pub fn peek_parser_u8_field(len: usize) -> impl Fn(&str) -> IResult<&str, u8> {
    move |input: &str| peek(parser_u8_field(len))(input)
}

pub fn digit_to_u8(input: &str) -> IResult<&str, u8> {
    map_opt(digit1, |x: &str| x.parse::<u8>().ok())(input)
}

pub fn parser_u8_field(len: usize) -> impl Fn(&str) -> IResult<&str, u8> {
    move |input: &str| -> IResult<&str, u8, nom::error::Error<&str>> {
        let (input2, value) = take(len)(input)?;

        let result: IResult<&str, u8, nom::error::Error<&str>> =
            map_opt(digit1, |found| u8::from_str_radix(found, 2).ok())(value);

        match result {
            Err(_) => Err(Error(make_error(input, ErrorKind::Digit))),
            Ok((_, value)) => Ok((input2, value)),
        }
    }
}

pub fn parser_u16_field(len: usize) -> impl Fn(&str) -> IResult<&str, u16> {
    move |input: &str| -> IResult<&str, u16, nom::error::Error<&str>> {
        let (input2, value) = take(len)(input)?;

        let result: IResult<&str, u16, nom::error::Error<&str>> =
            map_opt(digit1, |found| u16::from_str_radix(found, 2).ok())(value);

        match result {
            Err(_) => Err(Error(make_error(input, ErrorKind::Digit))),
            Ok((_, value)) => Ok((input2, value)),
        }
    }
}

pub fn parser_u32_field(len: usize) -> impl Fn(&str) -> IResult<&str, u32> {
    move |input: &str| -> IResult<&str, u32, nom::error::Error<&str>> {
        let (input2, value) = take(len)(input)?;

        let result: IResult<&str, u32, nom::error::Error<&str>> =
            map_opt(digit1, |found| u32::from_str_radix(found, 2).ok())(value);

        match result {
            Err(_) => Err(Error(make_error(input, ErrorKind::Digit))),
            Ok((_, value)) => Ok((input2, value)),
        }
    }
}

pub fn parser_i32_field(len: usize) -> impl Fn(&str) -> IResult<&str, i32> {
    move |input: &str| -> IResult<&str, i32, nom::error::Error<&str>> {
        map(parser_u32_field(len), |x| complement_2_u32(x, len as u32))(input)
    }
}

pub fn hex_to_u32(len: usize) -> impl Fn(&str) -> IResult<&str, u32> {
    fn from_hex(input: &str) -> Result<u32, std::num::ParseIntError> {
        u32::from_str_radix(input, 16)
    }

    move |input: &str| -> IResult<&str, u32, nom::error::Error<&str>> {
        map_res(take(len), from_hex)(input)
    }
}

pub fn parser_bool_field(input: &str) -> IResult<&str, bool> {
    alt((map(tag("0"), |_| false), map(tag("1"), |_| true)))(input)
}

#[cfg(test)]
#[cfg_attr(tarpaulin, ignore)]
mod tests {
    use crate::parsers::utils::parsers::complete::{
        digit_to_u8, parser_bool_field, parser_i32_field, parser_u16_field, parser_u32_field,
        parser_u8_field, peek_parser_u8_field,
    };
    use nom::error::{make_error, ErrorKind};
    use nom::Err::Error;

    #[test]
    fn should_parse_digit_u8() {
        assert_eq!(digit_to_u8("99"), Ok(("", 99u8)));
        assert_eq!(digit_to_u8("9a"), Ok(("a", 9u8)));
        assert_eq!(
            digit_to_u8("a"),
            Err(Error(make_error("a", ErrorKind::Digit)))
        );
    }

    #[test]
    fn should_parse_u8_field() {
        assert_eq!(parser_u8_field(2usize)("110"), Ok(("0", 3u8)));
        assert_eq!(
            parser_u8_field(3usize)("11"),
            Err(Error(make_error("11", ErrorKind::Eof)))
        );
        assert_eq!(
            parser_u8_field(3usize)("a111"),
            Err(Error(make_error("a111", ErrorKind::Digit)))
        );
        assert_eq!(
            parser_u8_field(6usize)("177jE;00008RsIn>pEQiphTP0<0"),
            Err(Error(make_error(
                "177jE;00008RsIn>pEQiphTP0<0",
                ErrorKind::Digit
            )))
        );
    }

    #[test]
    fn should_peek_u8_field() {
        assert_eq!(peek_parser_u8_field(2usize)("110"), Ok(("110", 3u8)));
        assert_eq!(
            peek_parser_u8_field(3usize)("11"),
            Err(Error(make_error("11", ErrorKind::Eof)))
        );
        assert_eq!(
            peek_parser_u8_field(3usize)("a111"),
            Err(Error(make_error("a111", ErrorKind::Digit)))
        );
        assert_eq!(
            peek_parser_u8_field(6usize)("177jE;00008RsIn>pEQiphTP0<0"),
            Err(Error(make_error(
                "177jE;00008RsIn>pEQiphTP0<0",
                ErrorKind::Digit
            )))
        );
    }

    #[test]
    fn should_parse_u16_field() {
        assert_eq!(parser_u16_field(2usize)("110"), Ok(("0", 3u16)));
        assert_eq!(
            parser_u16_field(16usize)("0000000111000111825427"),
            Ok(("825427", 455u16))
        );
        assert_eq!(
            parser_u16_field(3usize)("11"),
            Err(Error(make_error("11", ErrorKind::Eof)))
        );
        assert_eq!(
            parser_u16_field(3usize)("a111"),
            Err(Error(make_error("a111", ErrorKind::Digit)))
        );
        assert_eq!(
            parser_u16_field(6usize)("177jE;00008RsIn>pEQiphTP0<0"),
            Err(Error(make_error(
                "177jE;00008RsIn>pEQiphTP0<0",
                ErrorKind::Digit
            )))
        );
    }

    #[test]
    fn should_parse_u32_field() {
        assert_eq!(parser_u32_field(2usize)("110"), Ok(("0", 3u32)));
        assert_eq!(
            parser_u32_field(16usize)("0000000111000111825427"),
            Ok(("825427", 455u32))
        );
        assert_eq!(
            parser_u32_field(20usize)("10001111111101000000825427"),
            Ok(("825427", 589632u32))
        );
        assert_eq!(
            parser_u32_field(3usize)("11"),
            Err(Error(make_error("11", ErrorKind::Eof)))
        );
        assert_eq!(
            parser_u32_field(3usize)("a111"),
            Err(Error(make_error("a111", ErrorKind::Digit)))
        );
        assert_eq!(
            parser_u32_field(6usize)("177jE;00008RsIn>pEQiphTP0<0"),
            Err(Error(make_error(
                "177jE;00008RsIn>pEQiphTP0<0",
                ErrorKind::Digit
            )))
        );
    }

    #[test]
    fn should_parse_i32_field() {
        assert_eq!(parser_i32_field(2usize)("110"), Ok(("0", -1i32)));
        assert_eq!(parser_i32_field(2usize)("010"), Ok(("0", 1i32)));
        assert_eq!(
            parser_i32_field(16usize)("0000000111000111825427"),
            Ok(("825427", 455i32))
        );
        assert_eq!(
            parser_i32_field(21usize)("010001111111101000000825427"),
            Ok(("825427", 589632i32))
        );
        assert_eq!(
            parser_i32_field(28usize)("1111111101110000000011000000"),
            Ok(("", -589632i32))
        );
        assert_eq!(
            parser_i32_field(3usize)("11"),
            Err(Error(make_error("11", ErrorKind::Eof)))
        );
        assert_eq!(
            parser_i32_field(3usize)("a111"),
            Err(Error(make_error("a111", ErrorKind::Digit)))
        );
    }

    #[test]
    fn should_parse_bool_field() {
        assert_eq!(parser_bool_field("1"), Ok(("", true)));
        assert_eq!(parser_bool_field("01"), Ok(("1", false)));
        assert_eq!(
            parser_bool_field(""),
            Err(Error(make_error("", ErrorKind::Tag)))
        );
    }
}
