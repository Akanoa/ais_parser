// https://gpsd.gitlab.io/gpsd/AIVDM.html#_introduction
// http://www.gwenru.fr/index.php?a=ais

#[cfg(test)]
#[macro_use]
extern crate assert_matches;
extern crate serde;

extern crate nom;
pub mod parsers;