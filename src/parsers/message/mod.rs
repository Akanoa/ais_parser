use crate::parsers::message::type1::{parser_payload_type1, MessageType1};
use crate::parsers::message::type5::{parser_payload_type5, MessageType5};
use crate::parsers::utils::parsers::complete::peek_parser_u8_field;

pub mod type1;
pub mod type5;

#[derive(Debug)]
pub enum MessageType {
    PositionReportClassA(MessageType1),
    StaticAndVoyage(MessageType5),
    Unknown,
}

impl MessageType {
    pub fn from_payload(payload: &str) -> MessageType {
        let message_type = peek_parser_u8_field(6)(payload);

        if message_type.is_err() {
            return MessageType::Unknown;
        }

        let (payload, message_type) = message_type.unwrap_or(("", 0));
        match message_type {
            1 | 2 | 3 => match parser_payload_type1(payload) {
                Err(_e) => MessageType::Unknown,
                Ok((_, message)) => MessageType::PositionReportClassA(message),
            },
            5 => match parser_payload_type5(payload) {
                Err(_e) => MessageType::Unknown,
                Ok((_, message)) => MessageType::StaticAndVoyage(message),
            },
            _ => MessageType::Unknown,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::parsers::message::MessageType;
    use crate::parsers::utils::data::uncompress_payload;

    #[test]
    fn should_return_message_type_1() {
        let payload = uncompress_payload("177jE;00008RsIn>pEQiphTP0<0U").unwrap();

        let message = MessageType::from_payload(&payload[..]);

        assert_matches!(message, MessageType::PositionReportClassA(_));
    }

    #[test]
    fn should_return_message_type_5() {
        let payload = uncompress_payload(
            "54VE:802<@fL?HHsJ21<TiHE:1<P4@uN2222220t7B0;>C7<e?E25DTi0FH2Dk0CQ888881",
        )
        .unwrap();

        let message = MessageType::from_payload(&payload[..]);

        assert_matches!(message, MessageType::StaticAndVoyage(_));
    }

    #[test]
    fn should_return_unknown_message_invalid_type() {
        let payload = "877jE;00008RsIn>pEQiphTP0<0U";

        let message = MessageType::from_payload(&payload[..]);

        assert_matches!(message, MessageType::Unknown);
    }

    #[test]
    fn should_return_unknown_message() {
        let payload = uncompress_payload("877jE;00008RsIn>pEQiphTP0<0U").unwrap();

        let message = MessageType::from_payload(&payload[..]);

        assert_matches!(message, MessageType::Unknown);
    }
}
